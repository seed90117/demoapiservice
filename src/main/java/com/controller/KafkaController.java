package com.controller;

import com.service.SendKafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private SendKafkaService sendKafkaService;

    @RequestMapping(value = "/send/{topic}", method = RequestMethod.GET)
    public String send(@PathVariable String topic,
                       @RequestParam String message) {
        return String.valueOf(sendKafkaService.send(topic, message));
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        return "done";
    }
}
