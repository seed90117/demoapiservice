package com.controller;

import com.model.Response;
import com.model.Sample;
import com.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class BaseController {

    @Autowired
    private CommonService commonService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public void base() {}

    @PostMapping("/message")
    public Response message(@RequestBody Sample sample) {

        return commonService.message(sample);
    }
}
