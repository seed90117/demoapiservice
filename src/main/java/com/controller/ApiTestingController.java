package com.controller;

import com.model.ApiTesting;
import com.model.Response;
import com.service.ApiTestingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apiTesting")
public class ApiTestingController {

    @Autowired
    private ApiTestingService apiTestingService;

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public Response get() {
        return apiTestingService.get();
    }

    @RequestMapping(value = "getParam", method = RequestMethod.GET)
    public Response getParam(@RequestParam String name,
                             @RequestParam Integer number) {
        return apiTestingService.getParams(name, number);
    }

    @PutMapping("put")
    public Response put(@RequestBody ApiTesting apiTesting) {
        return apiTestingService.post(apiTesting);
    }

    @PutMapping("putParam")
    public Response putParam(@RequestParam Integer number,
                             @RequestParam Boolean isPass,
                             @RequestBody ApiTesting apiTesting) {
        return apiTestingService.postParams(apiTesting, number, isPass);
    }

    @PostMapping("post")
    public Response post(@RequestBody ApiTesting apiTesting) {
        return apiTestingService.post(apiTesting);
    }

    @PostMapping("postParam")
    public Response postParam(@RequestParam Integer number,
                              @RequestParam Boolean isPass,
                              @RequestBody ApiTesting apiTesting) {
        return apiTestingService.postParams(apiTesting, number, isPass);
    }

    @DeleteMapping("delete")
    public Response delete() {
        return apiTestingService.get();
    }

    @DeleteMapping("deleteParam/{name}")
    public Response deleteParam(@PathVariable("name") String name,
                              @RequestParam Integer number) {
        return apiTestingService.getParams(name, number);
    }

}
