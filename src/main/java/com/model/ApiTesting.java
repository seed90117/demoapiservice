package com.model;

public class ApiTesting {
    private String name;
    private String message;
    private Integer number;
    private Boolean isPass;

    public ApiTesting() {
        this.name = "Test";
        this.message = "Test for ApiTesting service";
        this.number = 93;
        this.isPass = true;
    }

    public ApiTesting(String name, Integer number) {
        this.name = name;
        this.number = number;
    }

    public ApiTesting(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public ApiTesting(String name, String message, Integer number, Boolean isPass) {
        this.name = name;
        this.message = message;
        this.number = number;
        this.isPass = isPass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean getIsPass() {
        return isPass;
    }

    public void setIsPass(Boolean pass) {
        isPass = pass;
    }

    @Override
    public String toString() {
        return "ApiTesting{" +
                "name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", number=" + number +
                ", isPass=" + isPass +
                '}';
    }
}
