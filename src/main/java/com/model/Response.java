package com.model;

public class Response {
    private String status;
    private String message;
    private Object context;

    public Response(String status) {
        this.status = status;
    }

    public Response(String status, Object context) {
        this.status = status;
        this.context = context;
    }

    public Response(String status, String message, Object context) {
        this.status = status;
        this.message = message;
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getContext() {
        return context;
    }

    public void setContext(Object context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "Response{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", context=" + context +
                '}';
    }
}
