package com.service;

import com.model.Response;
import com.model.Sample;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class CommonService {

    public String getWorkingDate(String inputDate, int count) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = simpleDateFormat.parse(inputDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.getTime().toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error");
            return null;
        }
    }

    public Response message(Sample sample) {
        return new Response("200", "success", sample);
    }
}
