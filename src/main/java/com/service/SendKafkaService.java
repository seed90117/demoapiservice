package com.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.Date;

@Service
public class SendKafkaService {

//    @Autowired
//    private KafkaTemplate<String, String> kafkaTemplate;
    private Gson gson = new GsonBuilder().create();
    //傳送訊息方法
    public boolean send(String topic, String msg) {
        try {
            Message message = new Message();
            message.setId(System.currentTimeMillis());
            message.setMsg(msg);
            message.setSendTime(new Date());
//            ListenableFuture<SendResult<String, String>> result = kafkaTemplate.send(topic, "test", gson.toJson(message));
//            result.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
//                @Override
//                public void onFailure(Throwable throwable) {
//
//                }
//
//                @Override
//                public void onSuccess(SendResult<String, String> stringStringSendResult) {
//                    System.out.println("Success " + stringStringSendResult.toString());
//                }
//            });

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
