package com.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DemoService {

    @Value("${test}")
    private String test;

    public String getKafkaServer() {
        return this.test;
    }
}
