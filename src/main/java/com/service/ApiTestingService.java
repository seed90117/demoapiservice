package com.service;

import com.model.ApiTesting;
import com.model.Response;
import org.springframework.stereotype.Service;

@Service
public class ApiTestingService {

    private final String successStatus = "200";

    private final String successMessage = "Success";

    public Response get() {
        return new Response(successStatus, successMessage, new ApiTesting());
    }

    public Response getParams(String name, Integer number) {
        ApiTesting apiTesting = new ApiTesting();
        apiTesting.setName(name);
        apiTesting.setNumber(number);
        return new Response(successStatus, successMessage, apiTesting);
    }

    public Response post(ApiTesting apiTesting) {
        return new Response(successStatus, successMessage, apiTesting);
    }

    public Response postParams(ApiTesting apiTesting, Integer number, Boolean isPass) {
        apiTesting.setNumber(number);
        apiTesting.setIsPass(isPass);
        return new Response(successStatus, successMessage, apiTesting);
    }
}
