package com;

import com.service.CommonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class BaseTest {

    @Autowired
    private CommonService commonService;

    @Test
    public void getWorkingDate() {
        commonService.getWorkingDate("2021/3/31", 3);
    }


}
