FROM library/ubi8/openjdk-8:1.3
ENV TZ='Asia/Taipei'
ARG JAR_FILE=target/DemoApiService-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/home/jboss/app.jar"]